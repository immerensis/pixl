package ui

import (
	"fyne.io/fyne/v2"
	"codeberg.org/immerensis/pixl/apptype"
	"codeberg.org/immerensis/pixl/pxcanvas"
	"codeberg.org/immerensis/pixl/swatch"
)

type AppInit struct {
	PixlCanvas *pxcanvas.PxCanvas
	PixlWindow fyne.Window
	State      *apptype.State
	Swatches   []*swatch.Swatch
}
